import React, { useState,useEffect } from 'react';

import clsx from 'clsx';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  Row,
  Col,
  CardBody,
  Card,
  CardHeader,
  Input,
  Badge,
  UncontrolledTooltip,
  Nav,
  NavItem,
  Button
} from 'reactstrap';
import { NavLink as NavLinkStrap } from 'reactstrap';
import PerfectScrollbar from 'react-perfect-scrollbar';
import avatar1 from '../../assets/images/avatars/av1.png';
import avatar2 from '../../assets/images/avatars/av2.png';
import avatar3 from '../../assets/images/avatars/av3.png';
import avatar4 from '../../assets/images/avatars/av4.png';
import avatar5 from '../../assets/images/avatars/av5.png';
import avatar6 from '../../assets/images/avatars/av1.png';
import avatar7 from '../../assets/images/avatars/av4.png';

import people2 from '../../assets/images/stock-photos/people-3.jpg';
import people1 from '../../assets/images/stock-photos/people-2.jpg';
import { connect } from "react-redux";

import {get_messages,create_msg} from '../../actions/msg';
function LivePreviewExample(prop) {
  
  useEffect(() => {
    prop.get_messages();
  },[]);
  const onFinish = (e) => {
    e.preventDefault()
    prop.create_msg(prop.auth.user.id,'bfbbcac9-4ee0-4769-b42c-6684d327bfd5',msg)
    // console.log("Received values of form: ", values);
    setMsg('');
    
  };
  const [msg, setMsg] = useState([]);
  const handleMsgChange = (e) => {
    e.preventDefault();
    setMsg(e.target.value);
  };

  const [messages, setMessage] = useState([]);
  const [isSidebarMenuOpen, setIsSidebarMenuOpen] = useState(false);
  const [isSidebarMenuOpen2, setIsSidebarMenuOpen2] = useState(false);
  
  const toggleSidebarMenu = () => setIsSidebarMenuOpen(!isSidebarMenuOpen);
  const toggleSidebarMenu2 = () => setIsSidebarMenuOpen2(!isSidebarMenuOpen2);

  const [activeTab, setActiveTab] = useState('1');

  const toggle = (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  const [inputBg, setInputBg] = useState(false);
  const toggleInputBg = () => setInputBg(!inputBg);

  return (
    <>
      <div className="app-inner-content-layout app-inner-content-layout-fixed">
        <div className="btn-md-pane d-lg-none px-4 order-0">
          <Button
            onClick={toggleSidebarMenu}
            size="sm"
            color="primary"
            className="p-0 btn-icon d-40">
            <FontAwesomeIcon icon={['fas', 'ellipsis-v']} />
          </Button>
          <Button
            onClick={toggleSidebarMenu2}
            size="sm"
            color="primary"
            className="p-0 btn-icon d-40">
            <FontAwesomeIcon icon={['fas', 'bars']} />
          </Button>
        </div>
        <div
          className={clsx(
            'app-inner-content-layout--sidebar bg-white app-inner-content-layout--sidebar__lg order-1',
            { 'layout-sidebar-open': isSidebarMenuOpen }
          )}>
          <PerfectScrollbar>
            <div className="px-4 pt-4">
              <Nav pills className="nav-neutral-primary flex-column">
                <NavItem className="d-flex pt-1 pb-3 justify-content-between">
                  <div className="text-uppercase font-size-sm text-primary font-weight-bold">
                    LES CHAINES
                  </div>
                  <div className="ml-auto font-size-xs">
                    <a
                      href="#/"
                      onClick={(e) => e.preventDefault()}
                      id="AddChannelTooltip1">
                      <FontAwesomeIcon icon={['fas', 'plus-circle']} />
                    </a>
                    <UncontrolledTooltip target="AddChannelTooltip1">
                      AJOUTER UNE NOUVELLE CHAINE
                    </UncontrolledTooltip>
                  </div>
                </NavItem>
                <NavItem>
                  <NavLinkStrap
                    href="#/"
                    onClick={(e) => e.preventDefault()}
                    active>
                    # LES PROJETS
                  </NavLinkStrap>
                </NavItem>
                <NavItem>
                  <NavLinkStrap href="#/" onClick={(e) => e.preventDefault()}>
                    # LES CLIENTS
                  </NavLinkStrap>
                </NavItem>
                <NavItem>
                  <NavLinkStrap href="#/" onClick={(e) => e.preventDefault()}>
                    <span># LES EMPLOYEES </span>
                    <Badge color="first" className="ml-auto">
                      23
                    </Badge>
                  </NavLinkStrap>
                </NavItem>
                <NavItem>
                  <NavLinkStrap href="#/" onClick={(e) => e.preventDefault()}>
                    # RESOURCES HUMAINES
                  </NavLinkStrap>
                </NavItem>
              </Nav>
              <div className="divider my-3" />
              <Nav pills className="nav-transparent flex-column">
                <NavItem className="pt-1 pb-3">
                  <div className="text-uppercase font-size-sm text-primary font-weight-bold">
                    La Liste des membres
                  </div>
                </NavItem>
                <NavItem>
                  <NavLinkStrap
                    href="#/"
                    onClick={(e) => e.preventDefault()}
                    className="px-0 text-black">
                    <div className="align-box-row w-100">
                      <div className="avatar-icon-wrapper avatar-icon-sm">
                        <Badge color="success" className="badge-circle">
                          En ligne
                        </Badge>
                        <div className="avatar-icon rounded-circle">
                          <img alt="..." src={avatar1} />
                        </div>
                      </div>
                      <div className="pl-2">
                        <span className="d-block font-size-sm font-weight-bold">
                          Sidi Mohamed
                          <span className="d-block font-weight-normal font-size-xs text-black-50">
                            (sidi123@gmail.com)
                          </span>
                        </span>
                      </div>
                      <div className="ml-auto">
                        <FontAwesomeIcon
                          icon={['fas', 'angle-right']}
                          className="text-right d-block font-size-sm"
                        />
                      </div>
                    </div>
                  </NavLinkStrap>
                </NavItem>
                <NavItem>
                  <NavLinkStrap
                    href="#/"
                    onClick={(e) => e.preventDefault()}
                    className="px-0 text-black">
                    <div className="align-box-row w-100">
                      <div className="avatar-icon-wrapper avatar-icon-sm">
                        <Badge color="danger" className="badge-circle">
                          Hors ligne
                        </Badge>
                        <div className="avatar-icon rounded-circle">
                          <img alt="..." src={avatar2} />
                        </div>
                      </div>
                      <div className="pl-2">
                        <span className="d-block font-size-sm font-weight-bold">
                          Mohamed Ali
                          <span className="d-block font-weight-normal font-size-xs text-black-50">
                            (Alimed@gmail.com)
                          </span>
                        </span>
                      </div>
                      <div className="ml-auto">
                        <FontAwesomeIcon
                          icon={['fas', 'angle-right']}
                          className="text-right d-block font-size-sm"
                        />
                      </div>
                    </div>
                  </NavLinkStrap>
                </NavItem>
                <NavItem>
                  <NavLinkStrap
                    href="#/"
                    onClick={(e) => e.preventDefault()}
                    className="px-0 text-black">
                    <div className="align-box-row w-100">
                      <div className="avatar-icon-wrapper avatar-icon-sm">
                        <Badge color="success" className="badge-circle">
                          En Ligne
                        </Badge>
                        <div className="avatar-icon rounded-circle">
                          <img alt="..." src={avatar3} />
                        </div>
                      </div>
                      <div className="pl-2">
                        <span className="d-block font-size-sm font-weight-bold">
                         Mohamedou Brahim
                          <span className="d-block font-weight-normal font-size-xs text-black-50">
                            (brahimMohamedou@yahoo.fr)
                          </span>
                        </span>
                      </div>
                      <div className="ml-auto">
                        <FontAwesomeIcon
                          icon={['fas', 'angle-right']}
                          className="text-right d-block font-size-sm"
                        />
                      </div>
                    </div>
                  </NavLinkStrap>
                </NavItem>
                <NavItem>
                  <NavLinkStrap
                    href="#/"
                    onClick={(e) => e.preventDefault()}
                    className="px-0 text-black">
                    <div className="align-box-row w-100">
                      <div className="avatar-icon-wrapper avatar-icon-sm">
                        <Badge color="warning" className="badge-circle">
                          Directeur Generale
                        </Badge>
                        <div className="avatar-icon rounded-circle">
                          <img alt="..." src={avatar4} />
                        </div>
                      </div>
                      <div className="pl-2">
                        <span className="d-block font-size-sm font-weight-bold">
                          Brahim El kory
                          <span className="d-block font-weight-normal font-size-xs text-black-50">
                            (ElkorySidiBrahim@gmail.com)
                          </span>
                        </span>
                      </div>
                      <div className="ml-auto">
                        <FontAwesomeIcon
                          icon={['fas', 'angle-right']}
                          className="text-right d-block font-size-sm"
                        />
                      </div>
                    </div>
                  </NavLinkStrap>
                </NavItem>
              </Nav>
              <div className="divider my-3" />
              <Nav pills className="nav-neutral-primary flex-column">
                <li className="nav-item pt-1 pb-3">
                  <div className="text-uppercase font-size-sm text-primary font-weight-bold">
                    Les Statistiques
                  </div>
                </li>
              </Nav>
              <Row className="font-size-xs">
                <Col lg="6">
                  <Card className="shadow-success-sm card-box text-center mb-4 p-3">
                    <div>
                      <FontAwesomeIcon
                        icon={['far', 'user']}
                        className="font-size-xxl text-success"
                      />
                    </div>
                    <div className="mt-2 line-height-sm">
                      <b className="font-size-lg">345</b>
                      <span className="text-black-50 d-block">Les membres</span>
                    </div>
                  </Card>
                </Col>
                <Col lg="6">
                  <Card className="shadow-danger-sm card-box text-center mb-4 p-3">
                    <div>
                      <FontAwesomeIcon
                        icon={['far', 'chart-bar']}
                        className="font-size-xxl text-danger"
                      />
                    </div>
                    <div className="mt-2 line-height-sm">
                      <b className="font-size-lg">2,693</b>
                      <span className="text-black-50 d-block">Les messages</span>
                    </div>
                  </Card>
                </Col>
              </Row>
              <Card className="shadow-first-sm card-box mb-4">
                <div className="card-indicator bg-first" />
                <CardBody className="px-4 py-3">
                  <div className="pb-3 d-flex justify-content-between">
                    <a href="#/" onClick={(e) => e.preventDefault()}>
                      Presentation du nouveau projet
                    </a>
                  </div>
                  <div className="d-flex align-items-center justify-content-start">
                    <Badge color="first" className="px-3">
                      En cours
                    </Badge>
                    <div className="font-size-sm text-danger px-2">
                      <FontAwesomeIcon
                        icon={['far', 'clock']}
                        className="mr-1"
                      />
                      14:22
                    </div>
                  </div>
                </CardBody>
              </Card>
              <Card className="shadow-success-sm card-box mb-4">
                <div className="card-indicator bg-success" />
                <CardBody className="px-4 py-3">
                  <div className="pb-3 d-flex justify-content-between">
                    <a href="#/" onClick={(e) => e.preventDefault()}>
                      Presenter le projet aux clients.
                    </a>
                  </div>
                  <div className="d-flex align-items-center justify-content-start">
                    <Badge color="success" className="px-3">
                      Fixer
                    </Badge>
                    <div className="font-size-sm text-dark px-2">
                      <FontAwesomeIcon
                        icon={['far', 'clock']}
                        className="mr-1"
                      />
                      09:41
                    </div>
                  </div>
                </CardBody>
              </Card>
            </div>
          </PerfectScrollbar>
        </div>
        <div className="app-inner-content-layout--main order-3 order-lg-2 card-box bg-secondary">
          <PerfectScrollbar>
            <CardHeader className="rounded-0 bg-white p-4 border-bottom">
              <div className="card-header--title">
                <small>Messagerie</small>
                <b>Parler Avec Mohamed</b>
              </div>
              <div className="card-header--actions">
                <Button
                  size="sm"
                  color="first"
                  className="btn-pill d-40 p-0"
                  id="SendMessageTooltip30">
                  <FontAwesomeIcon icon={['fas', 'plus']} />
                </Button>
                <UncontrolledTooltip target="SendMessageTooltip30">
                  Ajouter une conversation
                </UncontrolledTooltip>
              </div>
            </CardHeader>
            <div className="chat-wrapper p-3">
              {/* <div className="chat-item p-2 mb-2">
                <div className="align-box-row">
                  <div className="avatar-icon-wrapper avatar-icon-lg align-self-start">
                    <div className="avatar-icon rounded-circle shadow-none">
                      <img alt="..." src={avatar7} />
                    </div>
                  </div>
                  <div>
                    <div className="chat-box bg-gray-400 text-second">
                      <p>Hello, John.</p>
                      <p>This is Kenny. How are you?</p>
                    </div>
                    <small className="mt-2 d-block text-black-50">
                      <FontAwesomeIcon
                        icon={['far', 'clock']}
                        className="mr-1 opacity-5"
                      />
                      11:01 AM | Yesterday
                    </small>
                  </div>
                </div>
              </div>
              */}
              
                  {prop.messages && prop.messages.map(message => {
                      if((message.sender == prop.auth.user.id ) && (message.room == 'bfbbcac9-4ee0-4769-b42c-6684d327bfd5')){return <div key={message.id} className="chat-item p-2 mb-2">
                      <div className="align-box-row">
                        <div className="avatar-icon-wrapper avatar-icon-lg align-self-start">
                          <div className="avatar-icon rounded-circle shadow-none">
                            <img alt="..." src={avatar7} />
                          </div>
                        </div><div>
                      <div className="chat-box bg-gray-400 text-second">
                      <p>{message && message.text }</p>
                        
                      </div>
                      <small className="mt-2 d-block text-black-50">
                        <FontAwesomeIcon
                          icon={['far', 'clock']}
                          className="mr-1 opacity-5"
                        />
                       {message && message.date }
                      </small>
                    </div>
                    </div>
                    </div>}
                     if((message.sender != prop.auth.user.id ) && (message.room == 'bfbbcac9-4ee0-4769-b42c-6684d327bfd5')){return <div className="chat-item chat-item-reverse p-2 mb-2"  key={message.id}>
                     <div className="align-box-row flex-row-reverse">
                       <div className="avatar-icon-wrapper avatar-icon-lg align-self-start">
                         <div className="avatar-icon rounded-circle shadow-none">
                           <img alt="..." src={avatar3} />
                         </div>
                       </div>
                       <div>
                         <div className="chat-box bg-gray-400 text-second">
                         <p>{message && message.text }</p>
                           {/* <Card className="mt-3 mb-2 pt-2 pb-2 text-center">
                             <div>
                               <a href="#/" onClick={(e) => e.preventDefault()}>
                                 <img
                                   alt="..."
                                   className="img-fluid rounded m-1 shadow-sm"
                                   src={people1}
                                   width="54"
                                 />
                               </a>
                               <a href="#/" onClick={(e) => e.preventDefault()}>
                                 <img
                                   alt="..."
                                   className="img-fluid rounded m-1 shadow-sm"
                                   src={people2}
                                   width="54"
                                 />
                               </a>
                             </div>
                           </Card> */}
                         </div>
                         <small className="mt-2 d-block text-black-50">
                           <FontAwesomeIcon
                             icon={['far', 'clock']}
                             className="mr-1 opacity-5"
                           />
                           {message && message.date }
                         </small>
                       </div>
                     </div>
                   </div>}
                    
                      
                  })}
                
                 
             
              {/* <div className="chat-item chat-item-reverse p-2 mb-2">
                <div className="align-box-row flex-row-reverse">
                  <div className="mavatar-icon-wrapper avatar-icon-lg align-self-start">
                    <div className="avatar-icon rounded-circle shadow-none">
                      <img alt="..." src={avatar3} />
                    </div>
                  </div>
                  <div>
                    <div className="chat-box bg-gray-400 text-second">
                      <p>Almost forgot about your tasks.</p>
                      <p>
                        <b>Check the links below:</b>
                      </p>
                      <Card className="bg-second p-1 mt-3 mb-2">
                        <div className="text-center py-2">
                          <Button
                            color="link"
                            className="p-0 btn-icon bg-ripe-malin d-inline-block text-center text-white font-size-xl d-40 rounded-circle border-0 m-2"
                            id="MenuExampleTooltip11164">
                            <FontAwesomeIcon
                              icon={['far', 'gem']}
                              className="font-size-sm"
                            />
                          </Button>
                          <Button
                            color="link"
                            className="p-0 btn-icon bg-grow-early d-inline-block text-center text-white font-size-xl d-40 rounded-circle border-0 m-2"
                            id="MenuExampleTooltip11864">
                            <FontAwesomeIcon
                              icon={['far', 'building']}
                              className="font-size-sm"
                            />
                          </Button>
                          <Button
                            color="link"
                            className="p-0 btn-icon bg-arielle-smile d-inline-block text-center text-white font-size-xl d-40 rounded-circle border-0 m-2"
                            id="MenuExampleTooltip12564">
                            <FontAwesomeIcon
                              icon={['far', 'chart-bar']}
                              className="font-size-sm"
                            />
                          </Button>
                          <UncontrolledTooltip target="MenuExampleTooltip11164">
                            Menu example
                          </UncontrolledTooltip>
                          <UncontrolledTooltip target="MenuExampleTooltip11864">
                            Menu Example
                          </UncontrolledTooltip>
                          <UncontrolledTooltip target="MenuExampleTooltip12564">
                            Menu Example
                          </UncontrolledTooltip>
                        </div>
                      </Card>
                    </div>
                    <small className="mt-2 d-block text-black-50">
                      <FontAwesomeIcon
                        icon={['far', 'clock']}
                        className="mr-1 opacity-5"
                      />
                      11:03 AM | Yesterday
                    </small>
                  </div>
                </div>
              </div> */}
            </div>
            <div className="bg-white">
              <div className="card-footer p-0">
                <div className="d-block d-md-flex text-center text-md-left transition-base align-items-center justify-content-between py-3 px-4">
                  <div>
                    {/* <form onSubmit={onFinish} action="#">
                    <Button
                      type="submit"
                      size="sm"
                      color="neutral-primary"
                      className={clsx('d-inline-flex mr-2 btn-pill px-3 py-1', {
                        active: activeTab === '1'
                      })}
                      onClick={() => {
                        toggle('1');
                      }}>
                      <span className="btn-wrapper--label font-size-xs text-uppercase">
                        Create Post
                      </span>
                    </Button>
                    </form> */}
                    {/* <Button
                      size="sm"
                      color="neutral-primary"
                      className={clsx('d-inline-flex btn-pill px-3 py-1', {
                        active: activeTab === '3'
                      })}
                      onClick={() => {
                        toggle('3');
                      }}>
                      <span className="btn-wrapper--label font-size-xs text-uppercase">
                        Event
                      </span>
                    </Button> */}
                  </div>
                  
                </div>
                <div className="divider" />
                <div
                  className={clsx(
                    'd-flex align-items-center transition-base px-4 py-3',
                    { 'bg-secondary': inputBg }
                  )}>
                  <div className="avatar-icon-wrapper avatar-initials avatar-icon-lg mr-3">
                    <div className="avatar-icon bg-neutral-dark text-black">
                      Med
                    </div>
                    <Badge
                      color="success"
                      className="badge-position badge-position--bottom-center badge-circle"
                      title="Badge bottom center">
                      Online
                    </Badge>
                  </div>
                  <form onSubmit={onFinish} action="#" style={{width:"100%"}} autoComplete="off">
                  <Input
                    
                    onFocus={toggleInputBg}
                    onBlur={toggleInputBg}
                    className={clsx(
                      'transition-base border-0 pl-2 font-size-lg',
                      { 'pl-4': inputBg }
                    )}
                    bsSize="lg"
                    placeholder="Ecrire Votre Message Ici..."
                    name = "msg"
                    value = {msg}
                    onChange = {handleMsgChange}
                  />
                  </form>
                </div>
              </div>
            </div>
          </PerfectScrollbar>
        </div>
        <div
          className={clsx(
            'app-inner-content-layout--sidebar pos-r order-2 order-lg-3 bg-white app-inner-content-layout--sidebar__sm',
            { 'layout-sidebar-open': isSidebarMenuOpen2 }
          )}>
          <div className="text-uppercase font-size-sm text-primary font-weight-bold my-3 px-3">
            Liste des membres
          </div>
          <PerfectScrollbar>
            <Nav className="nav-neutral-first flex-column">
              <NavItem>
                <NavLinkStrap href="#/" onClick={(e) => e.preventDefault()}>
                  <div className="align-box-row">
                    <div className="avatar-icon-wrapper avatar-icon-sm">
                      <Badge color="danger" className="badge-circle">
                        Hors Ligne
                      </Badge>
                      <div className="avatar-icon rounded-circle">
                        <img alt="..." src={avatar2} />
                      </div>
                    </div>
                    <div className="pl-2">
                      <span className="d-block text-black font-size-sm font-weight-bold">
                        Sidi Ali
                        <div className="d-block text-black-50 font-size-xs font-weight-normal">
                          3 heures 
                        </div>
                      </span>
                    </div>
                  </div>
                </NavLinkStrap>
              </NavItem>
              <NavItem>
                <NavLinkStrap href="#/" onClick={(e) => e.preventDefault()}>
                  <div className="align-box-row">
                    <div className="avatar-icon-wrapper avatar-icon-sm">
                      <Badge color="success" className="badge-circle">
                        En Ligne
                      </Badge>
                      <div className="avatar-icon rounded-circle">
                        <img alt="..." src={avatar3} />
                      </div>
                    </div>
                    <div className="pl-2">
                      <span className="d-block text-black font-size-sm font-weight-bold">
                        Sidi Brahim
                        <div className="d-block text-black-50 font-size-xs font-weight-normal">
                          2 heures
                        </div>
                      </span>
                    </div>
                  </div>
                </NavLinkStrap>
              </NavItem>
              <NavItem>
                <NavLinkStrap href="#/" onClick={(e) => e.preventDefault()}>
                  <div className="align-box-row">
                    <div className="avatar-icon-wrapper avatar-icon-sm">
                      <Badge color="warning" className="badge-circle">
                        Moussa
                      </Badge>
                      <div className="avatar-icon rounded-circle">
                        <img alt="..." src={avatar4} />
                      </div>
                    </div>
                    <div className="pl-2">
                      <span className="d-block text-black font-size-sm font-weight-bold">
                        Moussa boukhary
                        <div className="d-block text-black-50 font-size-xs font-weight-normal">
                          3 heures
                        </div>
                      </span>
                    </div>
                  </div>
                </NavLinkStrap>
              </NavItem>
              <NavItem>
                <NavLinkStrap href="#/" onClick={(e) => e.preventDefault()}>
                  <div className="align-box-row">
                    <div className="avatar-icon-wrapper avatar-icon-sm">
                      <Badge color="danger" className="badge-circle">
                        Hors Ligne 
                      </Badge>
                      <div className="avatar-icon rounded-circle">
                        <img alt="..." src={avatar6} />
                      </div>
                    </div>
                    <div className="pl-2">
                      <span className="d-block text-black font-size-sm font-weight-bold">
                        Mohamed Ali
                        <div className="d-block text-black-50 font-size-xs font-weight-normal">
                          3 heures
                        </div>
                      </span>
                    </div>
                  </div>
                </NavLinkStrap>
              </NavItem>
              <NavItem>
                <NavLinkStrap href="#/" onClick={(e) => e.preventDefault()}>
                  <div className="align-box-row">
                    <div className="avatar-icon-wrapper avatar-icon-sm">
                      <Badge color="success" className="badge-circle">
                        En Ligne
                      </Badge>
                      <div className="avatar-icon rounded-circle">
                        <img alt="..." src={avatar1} />
                      </div>
                    </div>
                    <div className="pl-2">
                      <span className="d-block text-black font-size-sm font-weight-bold">
                        Inejih Abdel Rahman
                        <div className="d-block text-black-50 font-size-xs font-weight-normal">
                          5 heures
                        </div>
                      </span>
                    </div>
                  </div>
                </NavLinkStrap>
              </NavItem>
              <NavItem>
                <NavLinkStrap href="#/" onClick={(e) => e.preventDefault()}>
                  <div className="align-box-row">
                    <div className="avatar-icon-wrapper avatar-icon-sm">
                      <Badge color="danger" className="badge-circle">
                        Hors Ligne
                      </Badge>
                      <div className="avatar-icon rounded-circle">
                        <img alt="..." src={avatar2} />
                      </div>
                    </div>
                    <div className="pl-2">
                      <span className="d-block text-black font-size-sm font-weight-bold">
                        Ahmed Hassen
                        <div className="d-block text-black-50 font-size-xs font-weight-normal">
                          3 heures
                        </div>
                      </span>
                    </div>
                  </div>
                </NavLinkStrap>
              </NavItem>
              <NavItem>
                <NavLinkStrap href="#/" onClick={(e) => e.preventDefault()}>
                  <div className="align-box-row">
                    <div className="avatar-icon-wrapper avatar-icon-sm">
                      <Badge color="success" className="badge-circle">
                        En Ligne
                      </Badge>
                      <div className="avatar-icon rounded-circle">
                        <img alt="..." src={avatar3} />
                      </div>
                    </div>
                    <div className="pl-2">
                      <span className="d-block text-black font-size-sm font-weight-bold">
                        Mohamed Dj
                        <div className="d-block text-black-50 font-size-xs font-weight-normal">
                          2 heures
                        </div>
                      </span>
                    </div>
                  </div>
                </NavLinkStrap>
              </NavItem>
              <NavItem>
                <NavLinkStrap href="#/" onClick={(e) => e.preventDefault()}>
                  <div className="align-box-row">
                    <div className="avatar-icon-wrapper avatar-icon-sm">
                      <Badge color="warning" className="badge-circle">
                        Sidi Brahim
                      </Badge>
                      <div className="avatar-icon rounded-circle">
                        <img alt="..." src={avatar4} />
                      </div>
                    </div>
                    <div className="pl-2">
                      <span className="d-block text-black font-size-sm font-weight-bold">
                        Client
                        <div className="d-block text-black-50 font-size-xs font-weight-normal">
                          3 heures
                        </div>
                      </span>
                    </div>
                  </div>
                </NavLinkStrap>
              </NavItem>
              <NavItem>
                <NavLinkStrap href="#/" onClick={(e) => e.preventDefault()}>
                  <div className="align-box-row">
                    <div className="avatar-icon-wrapper avatar-icon-sm">
                      <Badge color="danger" className="badge-circle">
                        Hors Ligne
                      </Badge>
                      <div className="avatar-icon rounded-circle">
                        <img alt="..." src={avatar6} />
                      </div>
                    </div>
                    <div className="pl-2">
                      <span className="d-block text-black font-size-sm font-weight-bold">
                        Mahmoud
                        <div className="d-block text-black-50 font-size-xs font-weight-normal">
                          3 heures
                        </div>
                      </span>
                    </div>
                  </div>

                </NavLinkStrap>
              </NavItem>
              <NavItem>
                <NavLinkStrap href="#/" onClick={(e) => e.preventDefault()}>
                  <div className="align-box-row">
                    <div className="avatar-icon-wrapper avatar-icon-sm">
                      <Badge color="success" className="badge-circle">
                        En Ligne
                      </Badge>
                      <div className="avatar-icon rounded-circle">
                        <img alt="..." src={avatar5} />
                      </div>
                    </div>
                    <div className="pl-2">
                      <span className="d-block text-black font-size-sm font-weight-bold">
                        Moussa
                        <div className="d-block text-black-50 font-size-xs font-weight-normal">
                          2 heures
                        </div>
                      </span>
                    </div>
                  </div>
                </NavLinkStrap>
              </NavItem>
            </Nav>
          </PerfectScrollbar>
          <div className="app-content--sidebar__footer text-center p-3 d-block">
            <Button color="neutral-danger" size="sm">
              <span className="btn-wrapper--icon">
                <FontAwesomeIcon icon={['far', 'trash-alt']} />
              </span>
              <span className="btn-wrapper--label">Effacer L'historique</span>
            </Button>
          </div>
        </div>

        <div
          onClick={toggleSidebarMenu}
          className={clsx('sidebar-inner-layout-overlay', {
            active: isSidebarMenuOpen
          })}
        />
        <div
          onClick={toggleSidebarMenu2}
          className={clsx('sidebar-innerget_messages();-layout-overlay', {
            active: isSidebarMenuOpen2
          })}
        />
      </div>
    </>
  );
}
const mapStateToProps = state => {
  return {
    auth: state.auth,
    messages:state.msg.messages,
  };
};
export default connect(mapStateToProps,{get_messages,create_msg})(LivePreviewExample);
