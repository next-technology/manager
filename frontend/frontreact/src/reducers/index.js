import ThemeOptions from './ThemeOptions';
import auth from './auth';
import {combineReducers} from 'redux';
import msg  from './msg';
export default combineReducers({
  ThemeOptions,
  auth,
  msg,
});
