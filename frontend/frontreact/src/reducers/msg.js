import {CREATE_MESSAGE,ERROR_CREATE_MESSAGE,GET_MESSAGES,ERROR_GET_MESSAGES} from '../actions/type';

const initialState = {
    messages:[]
  };
  export default function (state = initialState, action) {
    switch (action.type) {
      case CREATE_MESSAGE:
      return {
        ...state,
        messages:[...state.messages,action.payload]
      };
      case GET_MESSAGES:
      return {
        ...state,
        messages:[...action.payload]
      };
      case ERROR_GET_MESSAGES:
      case ERROR_CREATE_MESSAGE:
      return state;
      default:
        return state;
    }
  }
  