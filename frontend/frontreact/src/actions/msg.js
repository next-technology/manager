import axios from 'axios';
import {CREATE_MESSAGE,ERROR_CREATE_MESSAGE,HOST,ERROR_GET_MESSAGES,GET_MESSAGES} from './type';

export const create_msg = (sender,room,text) => (dispatch) => {
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };
  
    const body = JSON.stringify({ sender, room,text });
    axios
      .post(HOST + "api_chat/create_msg/", body, config)
      .then((res) => {
        dispatch({
          type: CREATE_MESSAGE,
          payload: res.data,
        });
       
      })
      .catch((err) => {
        dispatch({
          type: ERROR_CREATE_MESSAGE,
        });
        console.log(err.response.data);
      });
  };

export const get_messages = () => (dispatch) => {
    const config = {
        headers: {
            "Content-Type": "application/json",
        },
    };

    axios.get(HOST + 'api_chat/',config)
        .then((res) =>{
          dispatch({
              type:GET_MESSAGES,
              payload:res.data,
          })
          
        })
        .catch((err) => {
            dispatch({
                type:ERROR_GET_MESSAGES,
            });
            console.log(err.response.data)
        })
}