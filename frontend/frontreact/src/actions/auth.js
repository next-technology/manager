import axios from "axios";
import {
  USER_LOADING,
  USER_LOADED,
  AUTH_ERROR,
  HOST,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  CLEAN_SESSION,
} from "./type";
export const login = (username, password) => (dispatch) => {
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };
  
    const body = JSON.stringify({ username, password });
    axios
      .post(HOST + "api/login/", body, config)
      .then((res) => {
        dispatch({
          type: LOGIN_SUCCESS,
          payload: res.data,
        });
       
      })
      .catch((err) => {
        dispatch({
          type: LOGIN_FAIL,
        });
        console.log(err.response.data);
      });
  };

  export const loadUser = () => (dispatch, getState) => {
    dispatch({ type: USER_LOADING });
    const access = getState().auth.access;
    //const refresh = getState().auth.refresh;
  
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };
    let url = "";
    if (access) {
      config.headers["Authorization"] = `JWT ${access}`;
      const tokenParts = JSON.parse(atob(access.split(".")[1]));
      
      url = "api/get_current_user/";
      
      //console.log(tokenParts);
      axios
        .get(HOST + url, config)
        .then((res) => {
          
          dispatch({
                  type: USER_LOADED,
                  payload:res.data,
                });
    
          // tokenToUser(res.data.user, config).then((result) => {
          //   if (result) {
          //     const { id, user, name, ...rest } = res.data;
          //     const payload = {
          //       user_id: res.data.user,
          //       profil_id: res.data.id,
          //       ...result,
          //       ...rest,
          //     };
          //     dispatch({
          //       type: USER_LOADED,
          //       payload,
          //     });
  
          //     console.log(payload);
          //   } else {
          //     dispatch({
          //       type: AUTH_ERROR,
          //     });
          //     //console.log(result);
          //   }
          // });
          //console.log(tokenUser);
        })
        .catch((err) => {
          dispatch({
            type: AUTH_ERROR,
          });
          console.log(err.response.data);
        });
    } else {
      dispatch({
        type: AUTH_ERROR,
      });
    }
  };
  export const logout = () => (dispatch, getState) => {
    const refresh_token = getState().auth.refresh;
    const body = { refresh_token };
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };
    axios
      .post(HOST + "api/logout/", body, config)
      .then(() => {
        dispatch({
          type: CLEAN_SESSION,
        });
        dispatch({
          type: LOGOUT,
        });
      })
      .catch((err) => console.log(err.response.data));
  };