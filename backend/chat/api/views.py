from rest_framework.generics import ListAPIView, CreateAPIView
from chat.models import Message, Room
from .serializers import MessageSerializer


class MessageListView(ListAPIView):
    queryset = Message.objects.all().order_by('date')
    serializer_class = MessageSerializer


class CreateMessage(CreateAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
