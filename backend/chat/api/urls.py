from django.urls import path

from .views import MessageListView, CreateMessage

urlpatterns = [
    path('', MessageListView.as_view()),
    path('create_msg/', CreateMessage.as_view())
]
